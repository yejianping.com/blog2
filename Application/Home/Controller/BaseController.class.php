<?php 
namespace Home\Controller;
use Think\Controller;
class BaseController extends Controller{
    function __construct(){
        parent::__construct();
    }
    public function _initialize(){
        $user = session("user","");
        if(is_array($user)){
            $this->assign('user',$user);
        }else{
            $this->assign('user',null);
        }
    }
}